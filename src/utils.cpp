// Copyright (C) 2009-2012 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2009-10-02
// Last changed: 2012-07-17

#include <boost/multi_array.hpp>
#include <ufc.h>
#include <ufc_geometry.h>
#include <dolfin/fem/FiniteElement.h>
#include "utils.h"

using namespace fenicssolid;

//-----------------------------------------------------------------------------
std::string fenicssolid::git_commit_hash()
{
  return FENICSSOLID_GIT_COMMIT_HASH;
}
//-----------------------------------------------------------------------------
void fenicssolid::compute_strain(Eigen::Matrix<double, 6, 1>& strain,
                                 const dolfin::FiniteElement& element,
                                 const double* vertex_coordinates,
                                 const double* coords,
                                 const std::vector<double>& expansion_coeff)
{
  // Zero strain vector.
  strain.setZero();

  // In 2D the values of components and derivatives in x and y direction
  // are:
  // derivatives = [N[0]_x, N[0]_y, N[1]_x, N[1]_y]
  // In 3D the values of components and derivatives in x, y and z
  // direction are:
  // derivatives = [N[0]_x, N[0]_y, N[0]_z, N[1]_x, N[1]_y, N[1]_z,     \
  //                N[2]_x, N[2]_y, N[2]_z]
  double derivatives[3];

  const std::size_t gdim = element.geometric_dimension();
  const std::size_t space_dim = element.space_dimension();
  for (unsigned int i = 0; i < space_dim; i++)
  {
    // Tabulate first derivatives of basis function i (and components) at
    // integration point 'ip'
    element.evaluate_basis_derivatives(i, 1, derivatives, coords,
                                       vertex_coordinates, 0);

    // Ux,x (eps_xx)
    strain(0) += derivatives[0]*expansion_coeff[i];

    // Uy,y (eps_yy)
    strain(1) += derivatives[1]*expansion_coeff[space_dim + i];

    // Ux,y + Uy,x (gamma_xy)
    strain(3) += derivatives[1]*expansion_coeff[i]
      + derivatives[0]*expansion_coeff[space_dim + i];

    // Add 3D strains
    if (gdim == 3)
    {
      // Uz,z (eps_zz)
      strain(2) += derivatives[2]*expansion_coeff[2*space_dim + i];

      // Ux,z + Uz,x (gamma_xz)
      strain(4) += derivatives[2]*expansion_coeff[i]
        + derivatives[0]*expansion_coeff[2*space_dim + i];

      // Uy,z + Uz,y (gamma_yz)
      strain(5) += derivatives[2]*expansion_coeff[space_dim + i]
        + derivatives[1]*expansion_coeff[2*space_dim + i];
    }
  }
}
//-----------------------------------------------------------------------------
void fenicssolid::compute_strain_reference(
  Eigen::Matrix<double, 6, 1>& strain,
  const boost::multi_array<double, 2>& derivatives,
  const double* vertex_coordinates,
  const std::vector<double>& expansion_coeff)
{
  // Zero strain vector
  strain.setZero();

  const std::size_t gdim = derivatives.shape()[1];

  // Compute Jacobian
  double J[9];
  double K[9];
  double detJ;
  if (gdim == 3)
  {
    compute_jacobian_tetrahedron_3d(J, vertex_coordinates);
    compute_jacobian_inverse_tetrahedron_3d(K, detJ, J);
  }
  else
  {
    compute_jacobian_triangle_2d(J, vertex_coordinates);
    compute_jacobian_inverse_triangle_2d(K, detJ, J);
  }

  const std::size_t space_dim = derivatives.shape()[0];

  double _derivatives[3];
  for (unsigned int dim = 0; dim < space_dim; dim++)
  {
    // Map derivatives to real space
    for (std::size_t i = 0; i < gdim; ++i)
    {
      _derivatives[i] = 0.0;
      for (std::size_t j = 0; j < gdim; ++j)
        _derivatives[i] += K[gdim*j + i]*derivatives[dim][j];
    }

    // Ux,x (eps_xx)
    strain(0) += _derivatives[0]*expansion_coeff[dim];

    // Uy,y (eps_yy)
    strain(1) += _derivatives[1]*expansion_coeff[space_dim + dim];

    // Ux,y + Uy,x (gamma_xy)
    strain(3) += _derivatives[1]*expansion_coeff[dim]
      + _derivatives[0]*expansion_coeff[space_dim + dim];

    // Add 3D strains
    if (gdim == 3)
    {
      // Uz,z (eps_zz)
      strain(2) += _derivatives[2]*expansion_coeff[2*space_dim + dim];

      // Ux,z + Uz,x (gamma_xz)
      strain(4) += _derivatives[2]*expansion_coeff[dim]
        + _derivatives[0]*expansion_coeff[2*space_dim + dim];

      // Uy,z + Uz,y (gamma_yz)
      strain(5) += _derivatives[2]*expansion_coeff[space_dim + dim]
        + _derivatives[1]*expansion_coeff[2*space_dim + dim];
    }
  }
}
//-----------------------------------------------------------------------------
void fenicssolid::compute_reference_x_3(
  std::vector<double>& X,
  const double* vertex_coordinates,
  const double* x)
{
  // Compute Jacobian
  double J[9];
  compute_jacobian_tetrahedron_3d(J, vertex_coordinates);

  // Compute Jacobian inverse and determinant
  double K[9];
  double detJ;
  compute_jacobian_inverse_tetrahedron_3d(K, detJ, J);

  // Compute constants
  const double C0 = vertex_coordinates[9]  + vertex_coordinates[6]
    + vertex_coordinates[3]  - vertex_coordinates[0];
  const double C1 = vertex_coordinates[10] + vertex_coordinates[7]
    + vertex_coordinates[4]  - vertex_coordinates[1];
    const double C2 = vertex_coordinates[11] + vertex_coordinates[8]
      + vertex_coordinates[5]  - vertex_coordinates[2];

  // Compute subdeterminants
  const double d_00 = J[4]*J[8] - J[5]*J[7];
  const double d_01 = J[5]*J[6] - J[3]*J[8];
  const double d_02 = J[3]*J[7] - J[4]*J[6];
  const double d_10 = J[2]*J[7] - J[1]*J[8];
  const double d_11 = J[0]*J[8] - J[2]*J[6];
  const double d_12 = J[1]*J[6] - J[0]*J[7];
  const double d_20 = J[1]*J[5] - J[2]*J[4];
  const double d_21 = J[2]*J[3] - J[0]*J[5];
  const double d_22 = J[0]*J[4] - J[1]*J[3];

  // Get coordinates and map to the reference (FIAT) element
  X.resize(3);
  X[0] = (d_00*(2.0*x[0] - C0) + d_10*(2.0*x[1] - C1) + d_20*(2.0*x[2] - C2)) / detJ;
  X[1] = (d_01*(2.0*x[0] - C0) + d_11*(2.0*x[1] - C1) + d_21*(2.0*x[2] - C2)) / detJ;
  X[2] = (d_02*(2.0*x[0] - C0) + d_12*(2.0*x[1] - C1) + d_22*(2.0*x[2] - C2)) / detJ;
}
//-----------------------------------------------------------------------------
//boost::multi_array<double, 3>
void fenicssolid::compute_basis_derivatives1(
  const dolfin::FiniteElement& element,
  const boost::multi_array<double, 2>& coordinates,
  const double* vertex_coordinates,
  boost::multi_array<double, 3>& derivs)
{
  const std::size_t gdim = element.geometric_dimension();
  dolfin_assert(coordinates.shape()[1] == gdim);
  const std::size_t num_points = coordinates.shape()[0];
  const std::size_t space_dim = element.space_dimension();

  // Compute Jacobian
  double J[9];
  if (gdim == 3)
    compute_jacobian_tetrahedron_3d(J, vertex_coordinates);
  else
   compute_jacobian_triangle_2d(J, vertex_coordinates);

  //boost::multi_array<double, 3>
  //  derivs(boost::extents[num_points][space_dim][gdim]);
  derivs.resize(boost::extents[num_points][space_dim][gdim]);
  std::fill(derivs.data(), derivs.data() + derivs.num_elements(), 0.0);

  // Loop over points
  for (std::size_t p = 0; p < num_points; ++p)
  {
    // Copy coordinates (need to figure out how to get a pointer)
    const boost::multi_array<double, 1> coord = coordinates[p];

    // Loop over basis functions
    for (unsigned int basis = 0; basis < space_dim; basis++)
    {
      // Tabulate first derivatives of basis function i (and
      // components) at integration point 'ip'
      double _derivs[3];
      element.evaluate_basis_derivatives(basis, 1, _derivs, coord.data(),
                                         vertex_coordinates, 0);

      // Map back to derivatives on reference cell
      for (std::size_t i = 0; i < gdim; ++i)
      {
        derivs[p][basis][i] = 0.0;
        for (std::size_t j = 0; j < gdim; ++j)
          derivs[p][basis][i] += J[gdim*j + i]*_derivs[j];
      }
    }
  }

  //return derivs;
}
//-----------------------------------------------------------------------------
