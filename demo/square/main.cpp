// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#include <dolfin.h>
#include <FenicsSolidMechanics.h>

// Switch between linear and quadratic elements
#include "../forms/p2_forms/Plas2D.h"
//#include "../forms/p1_forms/Plas2D.h"

using namespace dolfin;

// Displacement right end
class DBval : public Expression
{
public:

  DBval(const double& t) : t(t) {}

  void eval(Array<double>& values, const Array<double>& x) const
  { values[0] = t*x[1]; }

  const double& t;
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundaryX1 : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return std::abs(x[0] - 1.0) < DOLFIN_EPS; }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundaryX0 : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return x[0] < DOLFIN_EPS; }
};

int main()
{
  Timer timer("Total plasicity solver time");

  // Create mesh
  auto mesh = std::make_shared<UnitSquareMesh>(64, 64);

  // Young's modulus and Poisson's ratio
  double E = 20000.0;
  double nu = 0.3;

  // Time parameter
  double t = 0.0;

  // Elastic time step, always one step.
  double Edt  = 0.0095;

  // Load region 0, time step and number of steps
  double dt0 = 0.001;
  unsigned int dt0_steps = 3;

  // Load region 1, time step and number of steps
  double dt1 = -0.002;
  unsigned int dt1_steps =  1;

  // Load region 2, time step and number of steps
  double dt2 = 0.001;
  unsigned int dt2_steps =  4;

  // Source term, RHS
  auto f = std::make_shared<Constant>(0.0, 0.0);

  // Function spaces
  auto V = std::make_shared<Plas2D::FunctionSpace>(mesh);
  dolfin::cout << "Number of dofs: " << V->dim() << dolfin::endl;

  // Extract elements for stress and tangent
  std::shared_ptr<const FiniteElement> element_t;
  {
    Plas2D::BilinearForm::CoefficientSpace_t Vt(mesh);
    element_t = Vt.element();
  }

  auto Vs = std::make_shared<Plas2D::LinearForm::CoefficientSpace_s>(mesh);
  auto element_s = Vs->element();

  //Output3D::LinearForm::CoefficientSpace_u Veq(mesh);

  // Create boundary conditions (use SubSpace to apply simply
  // supported BCs)
  auto zero = std::make_shared<Constant>(0.0, 0.0);
  auto val = std::make_shared<DBval>(t);
  auto Vx = V->sub(0);

  auto dbX0 = std::make_shared<DirichletBoundaryX0>();
  auto dbX1 = std::make_shared<DirichletBoundaryX1>();
  auto bcX0 = std::make_shared<DirichletBC>(V, zero, dbX0);
  auto bcX1 = std::make_shared<DirichletBC>(Vx, val, dbX1);

  std::vector<std::shared_ptr<const DirichletBC>> bcs = {bcX0, bcX1};

  // Slope of hardening (linear) and hardening parameter
  const double E_t = 0.1*E;
  const double hardening_parameter = E_t/(1.0 - E_t/E);

  // Yield stress
  const double yield_stress = 200.0;

  // Solution function
  auto u = std::make_shared<Function>(V);

  // Object of class von Mises
  auto J2 = std::make_shared<const fenicssolid::VonMises>(E, nu, yield_stress,
                                                          hardening_parameter);

  // Constituive update
  auto constitutive_update
    = std::make_shared<fenicssolid::ConstitutiveUpdate>(*u, *element_s,
                                                        *Vs->dofmap(), *J2);

  // Create forms and attach functions
  auto tangent
    = std::make_shared<fenicssolid::QuadratureFunction>(*mesh, element_t,
                                                        constitutive_update,
                                                        constitutive_update->w_tangent());

  auto a = std::make_shared<Plas2D::BilinearForm>(V, V);
  a->t = tangent;

  auto L = std::make_shared<Plas2D::LinearForm>(V);
  L->f = f;

  auto stress = std::make_shared<fenicssolid::QuadratureFunction>(*mesh, element_s,
                                         constitutive_update->w_stress());
  L->s = stress;

  // Create PlasticityProblem
  fenicssolid::PlasticityProblem nonlinear_problem(a, L, *u, *tangent,
                                                   *stress, bcs, *J2);

  // Create nonlinear solver and set parameters
  dolfin::NewtonSolver nonlinear_solver;
  nonlinear_solver.parameters["convergence_criterion"] = "incremental";
  nonlinear_solver.parameters["maximum_iterations"]    = 50;
  nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
  nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

  // File names for output
  File file1("output/disp.pvd");
  File file2("output/eq_plas_strain.pvd");

  // Equivalent plastic strain for visualisation
  CellFunction<double> eps_eq(mesh);

  // Load-disp info
  unsigned int step = 0;
  unsigned int steps = dt0_steps + dt1_steps + dt2_steps + 1;
  while (step < steps)
  {
    // Use elastic tangent for first time step
    if (step == 0)
      t += Edt;
    else if (step < 1 + dt0_steps)
      t += dt0;
    else if (step < 1 + dt0_steps + dt1_steps)
      t += dt1;
    else if (step < 1 + dt0_steps + dt1_steps + dt2_steps)
      t += dt2;

    step++;
    std::cout << "step begin: " << step << std::endl;
    std::cout << "time: " << t << std::endl;

    // Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem, *u->vector());

    // Update variables
    constitutive_update->update_history();

    // Write output to files
    file1 << *u;
    constitutive_update->eps_p_eq().compute_mean(eps_eq);
    file2 << eps_eq;
  }
  cout << "Solution norm: " << u->vector()->norm("l2") << endl;

  timer.stop();
  dolfin::list_timings(dolfin::TimingClear::clear, {dolfin::TimingType::wall});

  return 0;
}
