install(CODE "MESSAGE(
\"----------------------------------------------------------------------------
FEniCS Solid Mechanics has now been installed in

    ${CMAKE_INSTALL_PREFIX}

Before rushing off to try the demos, don't forget to update your
environment variables. This can be done easily using the helper file
'fenics-solid-mechanics.conf' which sets the appropriate variables (for users of the
Bash shell).

To update your environment variables, run the following command:

    source ${CMAKE_INSTALL_PREFIX}/${FENICS_SOLID_MECHANICS_SHARE_DIR}/fenics-solid-mechanics.conf

For future reference, we recommend that you add this command to your
configuration (.bashrc, .profile or similar).
----------------------------------------------------------------------------\")")
